package models

type User struct {
	Id string
	Nick string
	Password string
}

type AllUsers struct {
	Length int
	Item []User
}

func (all AllUsers) GetLenght() int {
	return all.Length
}

func (user User) GetId() string {
	return user.Id
}

func (user *User) SetID(id string) {
	user.Id = id
}

func (user User) GetNick() string {
	return user.Nick
}

func (user *User) SetNick(nick string) {
	user.Nick = nick
}

func (user User) GetPassword() string {
	return user.Password
}

func (user *User) SetPassword(password string) {
	user.Password = password
}