package handlers

import (
	"crud_users/crud/pkg/controllers"
	"net/http"
	"log"
	"github.com/gorilla/mux"
	"time"
)

func SetUp() {
	router := mux.NewRouter()
	router.HandleFunc("/add", controllers.AddUser).Methods("POST")
	router.HandleFunc("/getAll", controllers.GetAll).Methods("GET")
	router.HandleFunc("/getUser", controllers.GetUser).Methods("GET")
	router.HandleFunc("/updateUser", controllers.UpdateUser).Methods("PUT")
	router.HandleFunc("/deleteAll", controllers.DeleteAll).Methods("DELETE")
	router.HandleFunc("/deleteUser", controllers.DeleteUser).Methods("DELETE")

	srv := &http.Server{
        Handler:      router,
        Addr:         "127.0.0.1:9090",
        WriteTimeout: 15 * time.Second,
        ReadTimeout:  15 * time.Second,
    }

    log.Fatal(srv.ListenAndServe())
}