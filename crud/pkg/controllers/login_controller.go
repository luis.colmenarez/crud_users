package controllers

import (
	"fmt"
	"crud_users/crud/pkg/services"
    "net/http"
    "crud_users/crud/pkg/models"
)

var listUsers models.AllUsers
var user models.User

func AddUser(w http.ResponseWriter, r *http.Request) {
    r.ParseForm()
    user.SetID(r.Form["id"][0])
    user.SetNick(r.Form["nick"][0])
    user.SetPassword(r.Form["passwd"][0])
    if(services.AddUser(user)) {
        fmt.Fprintln(w,"Usuario Agregado Correctamente")
    }
}

func GetAll(w http.ResponseWriter, r *http.Request) {
    listUsers, _ = services.GetAllUsers()
    fmt.Fprintln(w,"Usuarios: ", listUsers)
}

func GetUser(w http.ResponseWriter, r *http.Request) {
    r.ParseForm()
    user, _ = services.GetUser(r.Form["id"][0])
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {
    r.ParseForm()
    user.SetID(r.Form["id"][0])
    user.SetNick(r.Form["nick"][0])
    user.SetPassword(r.Form["passwd"][0])
    listUsers, _ = services.UpdateUser(user)
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
    r.ParseForm()
    listUsers, _ = services.DeleteUser(r.Form["id"][0])
}

func DeleteAll(w http.ResponseWriter, r *http.Request) {
    services.DeleteAllUsers()
}