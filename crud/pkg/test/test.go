package main

import (
    "fmt"
    "net/http"
    "net/url"
    "strconv"
    "strings"
)

func main() {
   post()
}

func post() {
    apiUrl := "http://localhost:9090"
    resource := "/add"
    data := url.Values{}
    data.Set("id", "3")
    data.Set("nick", "mario")
    data.Set("passwd", "asdas")

    u, _ := url.ParseRequestURI(apiUrl)
    u.Path = resource
    urlStr := u.String()

    client := &http.Client{}
    r, _ := http.NewRequest("POST", urlStr, strings.NewReader(data.Encode())) 
    r.Header.Add("Authorization", "auth_token=\"XXXXXXX\"")
    r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
    r.Header.Add("Content-Length", strconv.Itoa(len(data.Encode())))

    resp, _ := client.Do(r)
    fmt.Println(resp.Status)
}




