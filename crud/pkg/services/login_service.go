package services

import (
	"encoding/json"
	"crud_users/crud/pkg/models"
	"crud_users/crud/pkg/cache"
)

// Devuelve un array de UserStruct
func GetAllUsers() (models.AllUsers, error) {
	us := &models.AllUsers{}
	userJson, err := cache.Cache.Get("user")
	if(err != nil) {
		return *us, err
	}
	strJson, _ := userJson.(string)
	json.Unmarshal([]byte(string(strJson)), &us)
	return *us, nil
}

// Añade un nuevo usuario a la cache
func AddUser(item models.User) bool {
	_, exist := GetUser(item.GetId())
	flag := false
	if(!exist) {
		u, err := GetAllUsers()
		u.Length++
		u.Item = append(u.Item, models.User{Id: item.GetId(), Nick: item.GetNick(), Password: item.GetPassword()})
		jsonOut, err := json.Marshal(u)
		if err != nil {
			flag = false
		}
		cache.Cache.Set("user",string(jsonOut))
		flag =  true
	}
	return flag
}

// Añade un array de Users
func AddAll(users models.AllUsers) {
	for k := range users.Item {
		AddUser(users.Item[k])
	}
}

// Devuelve un usuario segun su ID
func GetUser(id string) (models.User, bool) {
	var user models.User
	users, _ := GetAllUsers()
	for k := range users.Item {
		if(users.Item[k].GetId() == id) {
			return users.Item[k], true
		}
	}
	return user, false
}

// Actualiza un Usuario
func UpdateUser(user models.User) (models.AllUsers, bool) {
	_, exist := GetUser(user.GetId())
	var users models.AllUsers
	var flag bool
	if(exist) {
		all, _ := GetAllUsers()
		for k := range all.Item {
			if(all.Item[k].GetId() != user.GetId()) {
				users.Item = append(users.Item,all.Item[k])
			} else {
				users.Item = append(users.Item,user)
				flag = true
			}
		}
		DeleteAllUsers()
		AddAll(users)
	}
	return users, flag
}

// Elimina un usuario y devuelve un array de usuarios modificado
func DeleteUser(id string) (models.AllUsers, bool) {
	var users models.AllUsers
	_, exist := GetUser(id)
	var flag bool
	if(exist) {
		all, _ := GetAllUsers()
		for k := range all.Item {
			if(all.Item[k].GetId() != id) {
				users.Item = append(users.Item,all.Item[k])
			} else {
				flag = true
			}
		}
		DeleteAllUsers()
		AddAll(users)
	}
	return users, flag
}

// Elimina los usuarios de la cache
func DeleteAllUsers()  {
	cache.Cache.Remove("user")
}

// Devuelve todos los usuarios como json string
func GetAllUsersStr() (string, bool) {
	userJson, err := cache.Cache.Get("user")
	if(err != nil) {
		return "", false
	}
	strJson, _ := userJson.(string)
	return strJson, true
}

// Devuelve todos un usuario como json string
func GetUserStr(id string) (string, bool) {
	users, _ := GetAllUsers()
	for k := range users.Item {
		if(users.Item[k].Id == id) {
			str, _ := json.Marshal(users.Item[k])
			return string(str), true
		}
	}
	return "", false
}