package cache

import (
	"github.com/bluele/gcache"
)

var Cache gcache.Cache

func init() {
	Cache = NewCache(20)
}

func NewCache(time int) gcache.Cache {
	return gcache.New(time).LRU().Build()
}